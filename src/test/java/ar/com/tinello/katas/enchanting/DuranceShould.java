package ar.com.tinello.katas.enchanting;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.Random;

import org.junit.Before;
import org.junit.Test;

public class DuranceShould {

	private RandomTenPercent ranprobabilityEnchantmentLosingdom;
	private Random enchantmentSelectionRandomized;
	private Durance durance;
	
	@Before
	public void setup() {
		this.ranprobabilityEnchantmentLosingdom = mock(RandomTenPercent.class);
		this.enchantmentSelectionRandomized = mock(Random.class);
		this.durance = new Durance(new Weapon("Dagger of the Nooblet", 5, 1.2f));
	}
	
	@Test
	public void durance_describe_weapon() {
		
		assertThat(durance.describeWeapon()).isEqualTo(
				"Dagger of the Nooblet\n" +
				"5 attack damage\n" +
				"1.2 attack speed\n"
				);
	}
	
	@Test
	public void random_ten_percent() {
		RandomTenPercent rand = new RandomTenPercent();
		int count = 0;
		for (int i = 0; i < 100; i++) {
			if(rand.generate() == 0) {
				count++;
			}
		}
		assertThat(count).isEqualTo(10);
	}
	
	@Test
	public void durance_weapon_can_be_enchanted_with_fire() {
		when(ranprobabilityEnchantmentLosingdom.generate()).thenReturn(1);
		durance.setProbabilityEnchantmentLosing(ranprobabilityEnchantmentLosingdom);
		when(enchantmentSelectionRandomized.nextInt(5)).thenReturn(Enchantments.FIRE.ordinal());
		durance.setEnchantmentSelectionRandomized(enchantmentSelectionRandomized);
		
		durance.enchant();
		
		assertThat(durance.describeWeapon()).isEqualTo(
				"Inferno Dagger of the Nooblet\n" +
				"5 attack damage\n" +
				"1.2 attack speed\n" +
				"+5 Fire Damage"
				);
	}
	
	@Test
	public void durance_weapon_can_be_enchanted_with_fire_only_one() {
		when(ranprobabilityEnchantmentLosingdom.generate()).thenReturn(1,1);
		durance.setProbabilityEnchantmentLosing(ranprobabilityEnchantmentLosingdom);
		when(enchantmentSelectionRandomized.nextInt(5)).thenReturn(Enchantments.FIRE.ordinal(), Enchantments.FIRE.ordinal());
		durance.setEnchantmentSelectionRandomized(enchantmentSelectionRandomized);
		durance.enchant();
		
		durance.enchant();
		
		assertThat(durance.describeWeapon()).isEqualTo(
				"Inferno Dagger of the Nooblet\n" +
				"5 attack damage\n" +
				"1.2 attack speed\n" +
				"+5 Fire Damage"
				);
	}
	
	@Test
	public void durance_weapon_can_be_enchanted_overflow() {
		when(ranprobabilityEnchantmentLosingdom.generate()).thenReturn(1,1,1,1);
		durance.setProbabilityEnchantmentLosing(ranprobabilityEnchantmentLosingdom);
		when(enchantmentSelectionRandomized.nextInt(5))
			.thenReturn(Enchantments.FIRE.ordinal(), Enchantments.AGILITY.ordinal(), Enchantments.ICE.ordinal(), Enchantments.STRENGTH.ordinal());
		durance.setEnchantmentSelectionRandomized(enchantmentSelectionRandomized);
		durance.enchant();
		durance.enchant();
		durance.enchant();
		
		durance.enchant();
		
		assertThat(durance.describeWeapon()).isEqualTo(
				"Angry Dagger of the Nooblet\n" +
				"5 attack damage\n" +
				"1.2 attack speed\n" +
				"+5 Strength\n" +
				"+5 Ice Damage\n" +
				"+5 Agility"
				);
	}
	
	@Test
	public void durance_lost_all_enchanted() {
		when(ranprobabilityEnchantmentLosingdom.generate()).thenReturn(4,3,2,1,0);
		durance.setProbabilityEnchantmentLosing(ranprobabilityEnchantmentLosingdom);
		when(enchantmentSelectionRandomized.nextInt(5))
			.thenReturn(Enchantments.FIRE.ordinal(), Enchantments.AGILITY.ordinal(), Enchantments.ICE.ordinal(), Enchantments.STRENGTH.ordinal(), 
					Enchantments.LIFESTEAL.ordinal());
		durance.setEnchantmentSelectionRandomized(enchantmentSelectionRandomized);
		durance.enchant();
		durance.enchant();
		durance.enchant();
		durance.enchant();
		
		durance.enchant();
		
		assertThat(durance.describeWeapon()).isEqualTo(
				"Ice Dagger of the Nooblet\n" +
				"5 attack damage\n" +
				"1.2 attack speed\n" +
				"+5 Ice Damage\n" +
				"+5 Agility"
				);
	}
	
	@Test
	public void durance_lost_all_enchantend_random() {
		do {
			durance.enchant();			
		} while (durance.isEnchanted());
		
		assertThat(durance.isEnchanted()).isFalse();
	}
}
