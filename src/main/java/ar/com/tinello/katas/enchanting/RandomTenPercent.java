package ar.com.tinello.katas.enchanting;

import java.util.LinkedList;
import java.util.Random;

public class RandomTenPercent {

	private LinkedList<Integer> accumulator;
	Random random;
	
	public RandomTenPercent() {
		accumulator = new LinkedList<>();
		random = new Random();
	}
	
	public int generate() {
		if (accumulator.size() == 10)
			accumulator.clear();
		
		boolean isExistInAccumulator;
		do {
			int rand = random.nextInt(10);
			isExistInAccumulator = accumulator.stream().anyMatch(i -> i == rand);
			if (!isExistInAccumulator)
				accumulator.addLast(rand);
			
		} while (isExistInAccumulator);
		
		return accumulator.getLast();
	}
	
}
