package ar.com.tinello.katas.enchanting;

public final class Weapon {
	private final String name;
    private final int attackDamage;
    private final float attackSpeed;
	
    public Weapon(String name, int attackDamage, float attackSpeed) {
		super();
		this.name = name;
		this.attackDamage = attackDamage;
		this.attackSpeed = attackSpeed;
	}

	public String getName() {
		return name;
	}

	public int getAttackDamage() {
		return attackDamage;
	}

	public float getAttackSpeed() {
		return attackSpeed;
	}
}
