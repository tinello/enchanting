package ar.com.tinello.katas.enchanting;

import java.util.LinkedList;
import java.util.Random;
import java.util.stream.Collectors;

public class Durance {
	
	private static final int LOST_ENCHANTMENT = 0;
	
	private Weapon weapon;
	private RandomTenPercent probabilityEnchantmentLosing;
	private LinkedList<Enchantments> enchantments;
	private Random enchantmentSelectionRandomized;

	public Durance(Weapon weapon) {
		this.weapon = weapon;
		this.probabilityEnchantmentLosing = new RandomTenPercent();
		this.enchantments = new LinkedList<>();
		this.enchantmentSelectionRandomized = new Random();
	}
	
	public void enchant() {
		if (probabilityEnchantmentLosing.generate() == LOST_ENCHANTMENT) {
			enchantments.removeFirst();
	 	} else {
			addEnchantment(Enchantments.values()[enchantmentSelectionRandomized.nextInt(5)]);
			lastThreEnchantment();
	 	}
	}

	private void lastThreEnchantment() {
		if (enchantments.size() > 3)
			enchantments.removeLast();
	}

	private void addEnchantment(Enchantments enchantment) {
		if (!enchantments.contains(enchantment))
			enchantments.addFirst(enchantment);
	}

	public String describeWeapon() {
		return (enchantments.isEmpty() ? "" : enchantments.get(0).getPrefix() + " ") + 
				weapon.getName() + "\n" + 
				weapon.getAttackDamage() + " attack damage\n" + 
				weapon.getAttackSpeed() + " attack speed\n" +
				(enchantments.isEmpty() ? "" : 
						enchantments
							.stream()
							.map(Enchantments::getAttributes)
							.collect(Collectors.joining("\n"))
				);
	}
	
	public boolean isEnchanted() {
		return !enchantments.isEmpty();
	}

	public void setProbabilityEnchantmentLosing(RandomTenPercent ranprobabilityEnchantmentLosingdom) {
		this.probabilityEnchantmentLosing = ranprobabilityEnchantmentLosingdom;
	}

	public void setEnchantmentSelectionRandomized(Random enchantmentSelectionRandomized) {
		this.enchantmentSelectionRandomized = enchantmentSelectionRandomized;
	}
	
}
