package ar.com.tinello.katas.enchanting;

public enum Enchantments {

	ICE("Ice", "+5 Ice Damage"),
    FIRE("Inferno", "+5 Fire Damage"),
    LIFESTEAL("Vampire", "+5 Lifesteal"),
    AGILITY("Quick", "+5 Agility"),
    STRENGTH("Angry", "+5 Strength");

	private String prefix;
    private String attributes;

    Enchantments(String prefix, String attributes) {
        this.prefix = prefix;
        this.attributes = attributes;
    }

	public String getPrefix() {
		return prefix;
	}

	public String getAttributes() {
		return attributes;
	}
}
